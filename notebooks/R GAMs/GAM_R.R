logjed <- function(x)
  return(log(x + 1))
expjed <- function(x)
  return(exp(x) - 1)

library(mgcv)

response = pickle_data$private

data = data.frame(pickle_data)

mod_gam <- gam(response ~  s(X0) + s(X1) + s(X2) + s(X3) + s(X4) + s(X5) + s(X6) + s(X7) + s(X8) + s(X9) + s(X10) + s(X11) + s(X12) + s(X13) + s(X14) + s(X15) + s(X16) + s(X17) + s(X18) + s(X19), 
               data=data, family=poisson)
summary(mod_gam)

reml_model <- gam(response ~  s(X0) + s(X1) + s(X2) + s(X3) + s(X4) + s(X5) + s(X6) + s(X7) + s(X8) + s(X9) + s(X10) + s(X11) + s(X12) + s(X13) + s(X14) + s(X15) + s(X16) + s(X17) + s(X18) + s(X19), 
                  data=data, family=binomial, method="REML", select="True")
summary(reml_model)

reml_model <- gam(response ~ s(X0) + s(X1) + s(X2) + s(X3) + s(X4) + s(X5) + s(X6) + s(X7) + s(X8) + s(X9) + s(X10) + s(X11) + s(X12) + s(X13) + s(X14) + s(X15) + s(X16) + s(X17) + s(X18) + s(X19),
                  data=data, family=binomial,select=TRUE, method="REML")
