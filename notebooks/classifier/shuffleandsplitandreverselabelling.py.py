import pandas as pd
from sklearn.model_selection import train_test_split
path = r"C:\Users\AMOR 1\Documents\Uni\Twitter-privacy-lda-topic-modelling\data\experiment.tsv"
df=pd.read_csv(path, sep='\t', header=None)
df.head(10)
#df[1] = 1 - df[1]
df.to_csv(path, sep='\t', header=None, index=None)
train, test = train_test_split(df , test_size=0.2, random_state=42)
test_path = r"C:\Users\AMOR 1\Documents\Uni\Twitter-privacy-lda-topic-modelling\data\test_data.pkl"
train_path = r"C:\Users\AMOR 1\Documents\Uni\Twitter-privacy-lda-topic-modelling\data\training_data.pkl"
test.to_pickle(test_path)
train.to_pickle(train_path)
