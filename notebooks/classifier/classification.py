import pandas as pd
import numpy as np
import pickle
from sklearn import svm
from sklearn.metrics import accuracy_score
import xgboost as xgb
from sklearn.metrics import accuracy_score
# import packages for hyperparameters tuning
from hyperopt import STATUS_OK, Trials, fmin, hp, tpe
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import f1_score
import warnings
warnings.filterwarnings(action='ignore', category=UserWarning)
import string
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer
from nltk.corpus import stopwords
stop = stopwords.words('english')
from sklearn.feature_extraction.text import CountVectorizer
from scipy.sparse import csr_matrix
from xgboost import XGBClassifier
import matplotlib.pyplot as plt
from scipy import stats
import seaborn as sns
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn.model_selection import RandomizedSearchCV, GridSearchCV


def get_index(list1, list2):
    """get all indeces of items in list1, which are also included in list2
    :param list1: list1
    :param list2: list2
    :type list1: list
    :type list2: list
    :rtype: list
    """

    ind = list()
    i = 0
    for i1 in list1:
        for i2 in list2:
            if i1==i2:
                ind.append(i)
        i += 1
    return ind


def lemmatize_text(text):
    """helper function that lemmatizes already tokenized text
    """

    lemmatizer = WordNetLemmatizer()
    return " ".join([lemmatizer.lemmatize(w, "v") for w in text])


def remove_punctuations(text):
    """removes all the symboles defined in punc

    :param text: string of which we want the symboles to be removed
    :type text: string
    :rtype: string
    """
    punc = '''!()-[]{};:'"\,<>./?@$%^&*_~'''
    for punctuation in punc:
        text = text.replace(punctuation, '')
    return text

# train_path = r"/home/atillmann/twitter-mining-master/data/training_data.pkl"
# train_path = r"C:\Users\AMOR 1\Documents\Uni\Twitter-privacy-lda-topic-modelling\data\training_data.pkl"
train_path = "/home/atillmann/twitter-mining-master/data/training_data.pkl"
train = pd.read_pickle(train_path)
X_train = np.array(train.drop("__label__", axis=1))
y_train = np.array(train["__label__"])
# test_path = r"/home/atillmann/twitter-mining-master/data/test_data.pkl"
# test_path = r"C:\Users\AMOR 1\Documents\Uni\Twitter-privacy-lda-topic-modelling\data\test_data.pkl"
test_path = "/home/atillmann/twitter-mining-master/data/test_data.pkl"
test = pd.read_pickle(test_path)
X_test = np.array(test.drop("__label__", axis=1))
y_test = np.array(test["__label__"])
#
#
# """support vector machine"""
# support = svm.SVC()
# support.fit(x_train, y_train)
# y_pred = support.predict(x_test)
#
# accuracy_score(y_test, y_pred)
# f1_score(y_test, y_pred)
#
"""XGBoost"""


space={'max_depth': hp.quniform("max_depth", 3, 18, 1),
        'gamma': hp.uniform ('gamma', 1,9),
        'reg_alpha' : hp.quniform('reg_alpha', 40,180,1),
        'reg_lambda' : hp.uniform('reg_lambda', 0,1),
        'colsample_bytree' : hp.uniform('colsample_bytree', 0.5,1),
        'min_child_weight' : hp.quniform('min_child_weight', 0, 10, 1),
        'n_estimators': 180,
        'seed': 0
    }


# def objective(space):
#     clf=xgb.XGBClassifier(
#                     n_estimators =space['n_estimators'], max_depth = int(space['max_depth']), gamma = space['gamma'],
#                     reg_alpha = int(space['reg_alpha']),min_child_weight=int(space['min_child_weight']),
#                     colsample_bytree=int(space['colsample_bytree']))
#
#     evaluation = [( X_train, y_train), ( X_test, y_test)]
#
#     clf.fit(X_train, y_train,
#             eval_set=evaluation, eval_metric="auc",
#             early_stopping_rounds=10,verbose=False)
#
#
#     pred = clf.predict(X_test)
#     accuracy = accuracy_score(y_test, pred>0.5)
#     print ("SCORE:", accuracy)
#     return {'loss': -accuracy, 'status': STATUS_OK }

#Choose all predictors except target & IDcols
xgb_pipeline = Pipeline([('scaler', StandardScaler()), ('classifier',XGBClassifier())])
params = {
        'min_child_weight': [1, 5, 10],
        'gamma': [0.5, 1, 1.5, 2, 5],
        'subsample': [0.6, 0.8, 1.0],
        'colsample_bytree': [0.6, 0.8, 1.0],
        'max_depth': [3, 4, 5]
        }
random_search = RandomizedSearchCV(xgb_pipeline, param_distributions=params, n_iter=100,
                                   scoring='f1_weighted', n_jobs=4, verbose=3, random_state=1001 )
random_search.fit(X_train,y_train)
#OR

#Grid Search
xgb_pipeline = Pipeline([('scaler', StandardScaler()), ('classifier',XGBClassifier())])
gbm_param_grid = {
    'classifier__learning_rate': np.array([0.01,0.001]),
    'classifier__n_estimators': np.array([100,200,300,400]),
    'classifier__subsample': np.array([0.7,0.8,0.9]),
    'classifier__max_depth': np.array([10,11,12,13,14,15,16,17]),
    'classifier__lambda': np.array([1]),
    'classifier__gamma': np.array([0]),
    'classifier__colsample_bytree': np.arange(0,1.1,.2)
    #'classifier__colsample_bytree': np.arange(0,1.1,.2)
}

grid_search = GridSearchCV(estimator=xgb_pipeline, param_grid=gbm_param_grid, n_jobs= -1,
                         scoring='f1_weighted', verbose=10)

grid_search.fit(X_train,y_train)

#Print out best parameters
print(random_search.best_params_)
print(grid_search.best_params_)
#Print out scores on validation set
print(random_search.score(X_test,y_test))
print(grid_search.score(X_test,y_test))
# xgboost=xgb.XGBClassifier(objective='binary:logistic', random_state=1,learning_rate=1.,use_label_encoder=False,
#                           colsample_bytree= 0.739778209181515, gamma= 4.63714060627425,
#                           max_depth= 12.0, min_child_weight= 6.0, reg_alpha= 163.0, reg_lambda= 0.5426149854832525)
# xgboost.fit(X_train, y_train)
xgboost.score(X_test,y_test)
x_test.shape
result_boolean = xgboost.predict(x_test)
result_proba = xgboost.predict_proba(x_test)
result_boolean2= (result_proba[:,1] > 0.5)
type(result_boolean2)

# """RandomForestClassifier"""
# random_forest = RandomForestClassifier(max_depth=20, random_state=0)
# random_forest.fit(x_train, y_train)
# random_forest_y_pred= random_forest.predict(x_test)
#
# accuracy_score(y_test, random_forest_y_pred)
# f1_score(y_test, random_forest_y_pred)


"""UK data classification"""
uk_data_path = r"C:\Users\AMOR 1\Documents\Uni\Twitter-privacy-lda-topic-modelling\data\uk_rawdata_20201226.pkl"
uk_data = pd.read_pickle(uk_data_path)
len(uk_data)
uk_data = pd.DataFrame(uk_data)[["id_str", "full_text"]]

uk_data["full_text"] = uk_data["full_text"].str.lower()
uk_data["full_text"] = uk_data["full_text"].apply(remove_punctuations)
uk_data["full_text"] = uk_data["full_text"].apply(word_tokenize)
uk_data["full_text"] = uk_data["full_text"].apply(lambda x: [item for item in x if item not in stop])
uk_data["full_text"] = uk_data["full_text"].apply(lemmatize_text)



vec1 = CountVectorizer()
X = vec1.fit_transform(uk_data["full_text"])
Y = X[:3,:]
Y.shape
corpus1_columns = vec1.get_feature_names()
corpus2_columns = train.drop("__label__", axis=1).columns.tolist()
indeces = get_index(corpus1_columns, corpus2_columns)

indeces2 = get_index(corpus2_columns, corpus1_columns)

_corpus1_columns = list()
for i in indeces:
    _corpus1_columns.append(corpus1_columns[i])
#df = pd.DataFrame(columns=corpus2_columns)

l = list()
l2 = list()
for i in range(0, (X.shape[0]-1000), 1000):
    Y = X[i:i+1000,:][:,indeces].toarray()
    Y1 = np.zeros((1000,5797))
    for i in range(len(indeces2)):
        Y1[:,indeces2[i]] = Y[:,i]
    # df = df.append(pd.DataFrame(Y.toarray()[:,indeces], columns=_corpus1_columns))
    # l = l + support.predict(Y1).tolist()
    result_proba = xgboost.predict_proba(Y1)
    result_boolean2= (result_proba[:,1] > 0.95)
    result_boolean3= (result_proba[:,1] < 0.05)
    l = l + result_boolean2.tolist()
    l2 = l2 + result_boolean3.tolist()
len ( l )

    # return X_new
uk_data=uk_data[:len(l)]
uk_data_docfeature_patch = r"C:\Users\AMOR 1\Documents\Uni\Twitter-privacy-lda-topic-modelling\data\uk_rawdata_20201226_docfeature.pkl"
# df.to_pickle(uk_data_docfeature_patch)
l = pd.DataFrame(l)
l.to_pickle(r"C:\Users\AMOR 1\Documents\Uni\Twitter-privacy-lda-topic-modelling\data\list_pos_clf_xgb.pkl")
l2 = pd.DataFrame(l2)
l2.to_pickle(r"C:\Users\AMOR 1\Documents\Uni\Twitter-privacy-lda-topic-modelling\data\list_neg_clf_xgb.pkl")
#
import pandas as pd
import numpy as np
l_new = pd.read_pickle(r"C:\Users\AMOR 1\Documents\Uni\Twitter-privacy-lda-topic-modelling\data\list_neg_clf_xgb.pkl")
sum(np.array(l_new))
# np.array(l[0]).sum()
# for i in range(len(l)):
#     if l[i] == 0:
#         uk_data = uk_data.drop(i)
#
# uk_data.to_pickle(r"C:\Users\AMOR 1\Documents\Uni\Twitter-privacy-lda-topic-modelling\data\uk_data_pos_clf.pkl")
