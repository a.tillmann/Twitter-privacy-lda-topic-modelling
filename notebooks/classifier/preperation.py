

"""
remove @XXX
tokenize
remove stopwords, symboles, punctuation
lowering
lemmatize
"""


# def remove_names(df):
#     """
#     removes all the @XXX
#
#     :param df: all the TWEETS
#     :type df: class pandas df
#     :return: df with removed @XXX
#     :rtype: class pandas df
#     """
import pandas as pd
from sklearn.model_selection import train_test_split
import string
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer
from nltk.corpus import stopwords
stop = stopwords.words('english')
from sklearn.feature_extraction.text import CountVectorizer



def lemmatize_text(text):
    """helper function that lemmatizes already tokenized text
    """

    lemmatizer = WordNetLemmatizer()
    return " ".join([lemmatizer.lemmatize(w, "v") for w in text])


def remove_punctuations(text):
    """removes all the symboles defined in punc

    :param text: string of which we want the symboles to be removed
    :type text: string
    :rtype: string
    """
    punc = '''!()-[]{};:'"\,<>./?@$%^&*_~'''
    for punctuation in punc:
        text = text.replace(punctuation, '')
    return text


path = r"C:\Users\AMOR 1\Documents\Uni\Twitter-privacy-lda-topic-modelling\data\experiment.tsv"
df=pd.read_csv(path, sep='\t', header=None)
# if __name__ == "__main__":
#     df = df.head(10)
df[0] = df[0].str.lower()
df[0] = df[0].str.replace("@xxx", "")
df[0] = df[0].apply(remove_punctuations)
df[0] = df[0].apply(word_tokenize)
df[0] = df[0].apply(lambda x: [item for item in x if item not in stop])
df[0] = df[0].apply(lemmatize_text)

vec = CountVectorizer()
X = vec.fit_transform(df[0])
df2 = pd.DataFrame(X.toarray(), columns=vec.get_feature_names())
df2["__label__"] = df[1]
train, test = train_test_split(df2 , test_size=0.2, random_state=42)
if __name__ == "__main__":
    test_path = r"C:\Users\AMOR 1\Documents\Uni\Twitter-privacy-lda-topic-modelling\data\test_data.pkl"
    train_path = r"C:\Users\AMOR 1\Documents\Uni\Twitter-privacy-lda-topic-modelling\data\training_data.pkl"
    test.to_pickle(test_path)
    train.to_pickle(train_path)
